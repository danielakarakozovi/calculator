package com.example.calculator


import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView


class MainActivity : AppCompatActivity() {
    private lateinit var text: TextView
    private var operand: Double = 0.0
    private var operation: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        text = findViewById(R.id.text)

    }

    @SuppressLint("SetTextI18n")
    fun numberClick(clickedView: View) {
        if (clickedView is TextView) {
            var result: String = text.text.toString()
            val number: String = clickedView.text.toString()
            if (result == "0")
                result = ""
            text.text = result + number
        }
    }

    fun operationClick(clickedView: View) {
        if (clickedView is TextView) {
            val result = text.text.toString()
            if (result.isNotEmpty()) {
                operand = result.toDouble()
            }
            operation = clickedView.text.toString()
            text.text = ""
        }
    }

    fun equalsClick(clickedView: View) {
        if (clickedView is TextView) {
            val secOperandText = text.text.toString()
            var secOperand = 0.0
            if (secOperandText.isNotEmpty()) {
                secOperand = secOperandText.toDouble()
            }

            when (operation) {
                "/" -> text.text = (operand / secOperand).toString()
                "*" -> text.text = (operand * secOperand).toString()
                "+" -> text.text = (operand + secOperand).toString()
                "-" -> text.text = (operand - secOperand).toString()
                "%" -> text.text = (operand % secOperand).toString()

            }
        }


    }
    fun acClick(clickedView: View) {
        if (clickedView is TextView) {
            text.text = ""
        }
    }
    fun delClick(clickedView: View) {
        if (clickedView is TextView) {
            val back = text.text.toString()
            text.text = back.dropLast(1)
        }
    }


}